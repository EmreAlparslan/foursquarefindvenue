**FoursquareFindVenue 1.0**

This is an application that is used to search for venues and get information about them.

* Find places nearby
* Read tips
* See venue in map image
* Learn how many people are there now!
* Supports 4.4+ Android Versions

### Libraries used in this application ###
* https://github.com/square/picasso
* https://github.com/google/gson
* https://github.com/medyo/Fancybuttons
* http://loopj.com/android-async-http/

### Screenshots ###
![findvenue_3000.jpg](https://bitbucket.org/repo/bpB9jg/images/587055004-findvenue_3000.jpg)