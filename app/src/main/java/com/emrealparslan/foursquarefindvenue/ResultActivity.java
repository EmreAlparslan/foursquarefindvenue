package com.emrealparslan.foursquarefindvenue;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.emrealparslan.foursquarefindvenue.Adapter.ResultAdapter;
import com.emrealparslan.foursquarefindvenue.Model.Venue;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResultActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private List<Venue> venues;
    private Toolbar toolbar;
    private TextView noResultTextView;
    private RecyclerView recyclerView;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.places));
        toolbar.setLogo(R.drawable.ic_nearme);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish this activity when back-arrow is clicked.
                finish();
            }
        });

        //get extras from previous activity
        Bundle extras = getIntent().getExtras();
        String jsonStr = extras.getString("json");

        //init venues list
        venues = new ArrayList<>();

        //initiliaze and create views
        initView();

        //parse the String JSON which is taken from previous activity
        parseJSON(jsonStr);
    }

    private void initView() {
        noResultTextView = (TextView) findViewById(R.id.no_result_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    //to show ProgressDialog
    private void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, "",
                getString(R.string.loading_list), true);
    }

    //parseJSON is a function that takes a String(JSON type) and creates objects according to it.
    private void parseJSON(String response) {
        showProgressDialog();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("venues")) {
                    venues.clear();
                    venues = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("venues");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Gson gson = new Gson();
                        Venue venue = gson.fromJson(String.valueOf(jsonArray.get(i)), Venue.class);
                        venues.add(venue);
                        Log.i("venue", venue.toString());
                    }
                }
                //init recyclerview
                recyclerView.setVisibility(View.VISIBLE);
                noResultTextView.setVisibility(View.GONE);
                ResultAdapter adapter = new ResultAdapter(this, venues);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
                recyclerView.setAdapter(adapter);
            } else {
                //No venues
                recyclerView.setVisibility(View.GONE);
                noResultTextView.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();
    }
}
