package com.emrealparslan.foursquarefindvenue.Dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.emrealparslan.foursquarefindvenue.Adapter.TipAdapter;
import com.emrealparslan.foursquarefindvenue.MainActivity;
import com.emrealparslan.foursquarefindvenue.Model.Photo;
import com.emrealparslan.foursquarefindvenue.Model.Tip;
import com.emrealparslan.foursquarefindvenue.Model.Venue;
import com.emrealparslan.foursquarefindvenue.R;
import com.emrealparslan.foursquarefindvenue.ResultActivity;
import com.emrealparslan.foursquarefindvenue.SimpleDividerItemDecoration;
import com.emrealparslan.foursquarefindvenue.Util;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emre on 11.12.2016.
 */

public class InfoDialog extends Dialog {

    private Venue venue;
    private List<Tip> tips;
    private ListView listView;
    private TextView venueNameTextView, venueAddressTextView, venueHereNowTextView;
    private ImageView imageView, mapView;
    private Context context;

    public InfoDialog(@NonNull Context context, Venue venue) {
        super(context);
        this.venue = venue;
        this.context = context;
        tips = new ArrayList<>();

        //disable cancel on touch outside of dialog
        setCanceledOnTouchOutside(true);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.info_layout);
        //to make oval corners
        this.getWindow().setBackgroundDrawableResource(R.drawable.oval_corner);

        //initiliaze and create views
        initView();

        //start with loading tips
        new TipsAsync().execute();
    }

    private void initView() {
        imageView = (ImageView) findViewById(R.id.venue_photo);
        venueNameTextView = (TextView) findViewById(R.id.venue_name_txtview);
        mapView = (ImageView) findViewById(R.id.map_view);
        listView = (ListView) findViewById(R.id.list_view);

        String staticMapImage = "https://maps.googleapis.com/maps/api/staticmap?center=" + String.valueOf(venue.location.lat) + "," + String.valueOf(venue.location.lng)
                + "&size=311x145&zoom=15&markers=color:red%7Clabel:C%7C" + String.valueOf(venue.location.lat) + "," +
                String.valueOf(venue.location.lng);

        Picasso.with(context).load(staticMapImage).into(mapView);

        venueNameTextView.setText(venue.name);
    }

    //displayPhoto is a function that takes a String(JSON type) and displays image in it.
    public void displayPhoto(String response) {
        String photoUrl = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("photos")) {
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONObject("photos").getJSONArray("items");
                    if(jsonArray.length() == 0){
                        imageView.setVisibility(View.GONE);
                    }else{
                        JSONObject photoObject = jsonArray.getJSONObject(0);
                        photoUrl = photoObject.getString("prefix") + String.valueOf(photoObject.getInt("width")) + "x"
                                + String.valueOf(photoObject.getInt("height")) + photoObject.getString("suffix");
                        imageView.setVisibility(View.VISIBLE);
                        Picasso.with(context).load(photoUrl).into(imageView);
                    }
                }
            } else {
                //no image
                imageView.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //parseTips is a function that takes a String(JSON type) and creates Tip objects and puts them in tips list.
    public void parseTips(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("tips")) {
                    tips.clear();
                    tips = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONObject("tips").getJSONArray("items");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Gson gson = new Gson();
                        Tip tip = gson.fromJson(String.valueOf(jsonArray.get(i)), Tip.class);
                        tips.add(tip);
                        Log.i("tip", tip.text.toString());
                    }
                }
                //init listview
                TipAdapter adapter = new TipAdapter(context, tips);
                listView.setAdapter(adapter);
            } else {
                //
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //TipAsync is used to get GET Request response from an URL.
    private class TipsAsync extends AsyncTask {

        String response;

        @Override
        protected Object doInBackground(Object[] objects) {
            response = Util.getRequest("https://api.foursquare.com/v2/venues/" +
                    venue.id + "/tips?client_id=33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2&client_secret=1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU&v=20130815&sort=recent&limit=15");
            return "";
        }

        @Override
        protected void onPostExecute(Object o) {
            if (response == null) {

            } else {
                parseTips(response);
                Log.i("response", response);
            }

            new PhotosAsync().execute();
        }
    }

    //PhotosAsync is used to get GET Request response from an URL.
    private class PhotosAsync extends AsyncTask {

        String response;

        @Override
        protected Object doInBackground(Object[] objects) {
            response = Util.getRequest("https://api.foursquare.com/v2/venues/" + venue.id +
                    "/photos?client_id=33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2&client_secret=1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU&v=20130815&limit=1");
            return "";
        }

        @Override
        protected void onPostExecute(Object o) {
            if (response == null) {

            } else {
                displayPhoto(response);
                Log.i("response", response);
            }
        }
    }
}
