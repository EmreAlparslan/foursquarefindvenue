package com.emrealparslan.foursquarefindvenue;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        //Creating a timer
        CountDownTimer timer = new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                //Start MainActivity after 3 seconds.
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
            }
        };

        //Start timer
        timer.start();

    }
}
