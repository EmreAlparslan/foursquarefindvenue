package com.emrealparslan.foursquarefindvenue.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emrealparslan.foursquarefindvenue.Dialog.InfoDialog;
import com.emrealparslan.foursquarefindvenue.Model.Venue;
import com.emrealparslan.foursquarefindvenue.R;

import java.util.List;

/**
 * Created by Emre on 11.12.2016.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.Holder> {

    private List<Venue> venues;
    private Context ctx;

    public ResultAdapter(Context ctx, List<Venue> venues){
        this.ctx = ctx;
        this.venues = venues;
    }

    @Override
    public ResultAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_item, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(ResultAdapter.Holder holder, int position) {
        final Venue venue = venues.get(position);
        if(venue.name != null){
            holder.name.setText(venue.name);
        }
        holder.location.setText(venue.location.address + ", " + venue.location.city);
        holder.herenow.setText(venue.hereNow.count + " " + ctx.getString(R.string.here_now));
        holder.country.setText(venue.location.country);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InfoDialog infoDialog = new InfoDialog(ctx, venue);
                infoDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return venues.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public TextView name, location, herenow, country;
        public RelativeLayout relativeLayout;

        public Holder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.venue_name);
            location = (TextView) view.findViewById(R.id.venue_location);
            herenow = (TextView) view.findViewById(R.id.venue_current_people);
            country = (TextView) view.findViewById(R.id.venue_country);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_layout);
        }
    }
}
