package com.emrealparslan.foursquarefindvenue.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.emrealparslan.foursquarefindvenue.Model.Tip;
import com.emrealparslan.foursquarefindvenue.R;

import java.util.List;

/**
 * Created by Emre on 11.12.2016.
 */

public class TipAdapter extends BaseAdapter {

    private Context context;
    private List<Tip> tips;
    private static LayoutInflater inflater = null;

    public TipAdapter(Context context, List<Tip> tips) {
        this.context = context;
        this.tips = tips;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tips.size();
    }

    @Override
    public Object getItem(int i) {
        return tips.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.tip_item, null);
        holder.user = (TextView) rowView.findViewById(R.id.user_name);
        holder.tip = (TextView) rowView.findViewById(R.id.tip);

        Tip tip = tips.get(i);

        holder.user.setText(tip.user.firstName + " " + tip.user.lastName);
        holder.tip.setText(tip.text);

        return rowView;
    }

    public class Holder
    {
        TextView user;
        TextView tip;
    }
}
