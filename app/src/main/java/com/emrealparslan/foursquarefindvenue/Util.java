package com.emrealparslan.foursquarefindvenue;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.emrealparslan.foursquarefindvenue.Model.Tip;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;

/**
 * Created by Emre on 10.12.2016.
 */

public class Util {

    //isVenueTypeValid is a function that takes an String and returns true if it is valid.
    public static int isVenueTypeValid(String type){
        if(type.length() >= 3 && type.matches("[a-zA-Z]+")){
            return 0;
        }else if(type.length() < 3){
            return 1;
        }else if(!type.matches("[a-zA-Z]+")){
            return 2;
        }else{
            return 3;
        }
    }

    //getRequest is a function that takes an String and returns GET Request response of it.
    public static String getRequest(String url) {

        // string buffers the url
        StringBuffer buffer_string = new StringBuffer(url);
        String replyString = "";

        // instanciate an HttpClient
        HttpClient httpclient = HttpClientBuilder.create().build();
        // instanciate an HttpGet
        HttpGet httpget = new HttpGet(buffer_string.toString());

        try {
            // get the responce of the httpclient execution of the url
            HttpResponse response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();

            // buffer input stream the result
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            // the result as a string is ready for parsing
            replyString = new String(baf.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // trim the whitespaces
        return replyString.trim();
    }

    //isConnected is a function that takes an context and returns true if device is connected to internet.
    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    //replaceSpacesWithUnderscore is a function that takes an String and replaces spaces in String with underscores and returns it.
    public static String replaceSpacesWithUnderscore(String str){
        return str.replaceAll(" ", "_");
    }

}
