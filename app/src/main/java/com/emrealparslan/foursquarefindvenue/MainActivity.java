package com.emrealparslan.foursquarefindvenue;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.emrealparslan.foursquarefindvenue.Model.Venue;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;
import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout venueTypeInputLayout, cityInputLayout;
    private EditText venueTypeEdiText, cityEditText;
    private FancyButton searchButton;
    private List<Venue> venues;
    private ProgressDialog progressDialog;
    private boolean isCityInputEmpty;
    private String venueTypeStr, cityRegionStr;
    private Location currentLocation;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        venues = new ArrayList<>();
        isCityInputEmpty = true;
        initView();
    }

    private void initView(){
        venueTypeInputLayout = (TextInputLayout)findViewById(R.id.type_input_layout);
        cityInputLayout = (TextInputLayout)findViewById(R.id.city_input_layout);
        venueTypeEdiText = (EditText)findViewById(R.id.type_edit_text);
        cityEditText = (EditText)findViewById(R.id.city_edit_text);
        searchButton = (FancyButton)findViewById(R.id.search_button);

        venueTypeEdiText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                venueTypeInputLayout.setErrorEnabled(false);
                return false;
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.isConnected(context)){
                    venueTypeStr = venueTypeEdiText.getText().toString().trim();
                    cityRegionStr = Util.replaceSpacesWithUnderscore(cityEditText.getText().toString().trim());
                    int venueTypeValidation = Util.isVenueTypeValid(venueTypeStr);
                    switch (venueTypeValidation){
                        case 0:
                            //correct
                            showProgressDialog();
                            if(cityRegionStr.length() > 0){
                                //search by city/region
                                isCityInputEmpty = false;
                            }else{
                                //search nearby
                                currentLocation = getLastKnownLocation();
                                isCityInputEmpty = true;
                            }
                            requestApi();
                            break;
                        case 1:
                            //less than 3
                            showErrorDialog(getString(R.string.error_less_than_three));
                            break;
                        case 2:
                            //contains character that's not letter
                            showErrorDialog(getString(R.string.error_only_letters));
                            break;
                        case 3:
                            //other error
                            break;
                    }
                }else{
                    showNoConnectionDialog();
                }
            }
        });
    }

    private void requestApi(){
        new Foursquare().execute();
    }

    private void showProgressDialog(){
        progressDialog = ProgressDialog.show(this, "",
                getString(R.string.loading_list), true);
    }

    private void showErrorDialog(String msg){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(msg)
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showNoConnectionDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.no_connection))
                .setMessage(getString(R.string.please_connect))
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                        overridePendingTransition(R.anim.right_to_left, R.anim.right_to_left_slide_out);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private class Foursquare extends AsyncTask {

        String response;

        @Override
        protected Object doInBackground(Object[] objects) {
            // make Call to the url
            if(isCityInputEmpty){
                if(currentLocation != null){
                    response = Util.getRequest("https://api.foursquare.com/v2/venues/search?client_id=33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2&client_secret=1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU&v=20130815&ll=" + currentLocation.getLatitude() +
                            "," + currentLocation.getLongitude() + "&query=" + venueTypeStr);
                }

            }else{
                response = Util.getRequest("https://api.foursquare.com/v2/venues/search?client_id=33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2&client_secret=1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU&v=20130815&near=" + cityRegionStr + "&query=" + venueTypeStr);
            }
            return "";
        }

        @Override
        protected void onPostExecute(Object o) {
            if (response == null) {

            } else {
                Log.i("response", response);
                //Pass data to ResultActivity1 and startActivity
                progressDialog.dismiss();
                Intent i = new Intent(MainActivity.this, ResultActivity.class);
                Bundle extras = new Bundle();
                extras.putString("json", response);
                i.putExtras(extras);
                startActivity(i);
                overridePendingTransition(R.anim.right_to_left, R.anim.right_to_left_slide_out);
            }
        }
    }

    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
