package com.emrealparslan.foursquarefindvenue.Model;

/**
 * Created by Emre on 11.12.2016.
 */

public class Photo {
    public String id;
    public String prefix, suffix;
    public int width, height;
}
