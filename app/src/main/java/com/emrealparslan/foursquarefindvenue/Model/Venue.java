package com.emrealparslan.foursquarefindvenue.Model;

import java.util.List;

/**
 * Created by Emre on 10.12.2016.
 */

public class Venue {
    public String id;
    public String name;
    public List<Category> categories;
    public Location location;
    public HereNow hereNow;
}
