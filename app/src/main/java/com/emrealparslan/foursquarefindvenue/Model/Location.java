package com.emrealparslan.foursquarefindvenue.Model;

/**
 * Created by Emre on 10.12.2016.
 */

public class Location {
    public String address;
    public double lat, lng;
    public int distance;
    public String city;
    public String country;
}
